//External Helpers
const { messageAllThreads } = require('../../world/threadManager');

//Internal
const configs = {};

//Module
const balanceTracker = {};

//Helpers
balanceTracker.set = ({ mod, contentKey, config }) => {
	if (!configs[mod])
		configs[mod] = {};

	configs[mod][contentKey] = config;

	const fnKey = `${mod}${contentKey[0].toUpperCase()}${contentKey.substr(1)}`;

	Object.defineProperty(balanceTracker, fnKey, {
		get: () => {
			return configs[mod][contentKey];
		},
		configurable: true
	}); 

	messageAllThreads({
		threadModule: 'balanceTracker',
		method: 'set',
		data: {
			mod,
			contentKey,
			config
		}
	});
};

balanceTracker.get = ({ mod, contentKey }) => {
	return configs[mod]?.[contentKey] ?? {};
};

balanceTracker.getAll = () => configs;

//Module
module.exports = balanceTracker;
