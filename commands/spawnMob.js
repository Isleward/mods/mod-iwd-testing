const objects = require('../../../objects/objects');

const spawnMob = function (config) {
	const id = 'squiggle' ?? config.id ?? config;
	const faction = 'players' ?? config.faction ?? 'hostile';

	objects.buildObject({
		x: this.obj.x,
		y: this.obj.y,
		mobConfig: {
			id,
			faction,
			walkDistance: 3,
			spawnedBy: this.obj
		}
	});
};

module.exports = spawnMob;
