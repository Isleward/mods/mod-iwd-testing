const itemGenerator = require('../../../items/generator');

const doubleSlots = ['finger'];

const blueprints = [{
	spirit: 'bear',
	build: 'tank',
	itemStats: {
		entry: ['str', 'vit'],
		low: ['str', 'str', 'vit', 'regenHp'],
		mid: ['str', 'str', 'str', 'vit', 'vit', 'regenHp'],
		high: ['str', 'str', 'str', 'vit', 'vit', 'vit', 'regenHp', 'regenHp']
	},
	itemTypes: {
		head: 'Helmet',
		neck: 'Pendant',
		chest: 'Breastplate',
		finger: 'Signet',
		hands: 'Gauntlets',
		waist: 'Belt',
		legs: 'Legplates',
		feet: 'Steel Boots',
		trinket: 'Forged Ember',
		oneHanded: 'Sword',
		offHand: 'Gilded Shield'
	},
	runes: ['slash', 'charge', 'charge', 'innervation'],
	extraConfig: {
		oneHanded: {
			spellQuality: 4
		}
	},
	passives: [
		12, 16, 106, 26, 20, 7, 21,
		22, 11, 10, 14, 18, 17, 23,
		13, 19, 0, 1, 2, 62, 104
	],
	itemQuality: 4
}, {
	spirit: 'bear',
	build: 'dps',
	itemStats: {
		entry: ['str', 'vit'],
		low: ['str', 'str', 'vit', 'regenHp'],
		mid: [
			'str',
			'str',
			'physicalPercent',
			'addCritChance',
			'addCritChance',
			'addCritMultiplier'
		],
		high: [
			'physicalPercent',
			'addAttackDamage',
			'addAttackDamage',
			'addAttackDamage',
			'addAttackDamage',
			'addCritChance',
			'addCritChance',
			'addCritMultiplier'
		]
	},
	itemTypes: {
		head: 'Helmet',
		neck: 'Pendant',
		chest: 'Breastplate',
		finger: 'Signet',
		hands: 'Gauntlets',
		waist: 'Belt',
		legs: 'Legplates',
		feet: 'Steel Boots',
		trinket: 'Forged Ember',
		twoHanded: 'Axe'
	},
	runes: ['slash', 'charge', 'whirlwind', 'innervation'],
	extraConfig: {
		oneHanded: {
			spellQuality: 4
		}
	},
	passives: [
		12, 16, 106, 26, 20, 7, 21,
		22, 11, 10, 14, 18, 17, 23,
		13, 19, 0, 1, 2, 62, 104
	],
	itemQuality: 4
}, {
	spirit: 'owl',
	build: 'dps',
	itemStats: {
		entry: ['int', 'int'],
		low: ['int', 'int', 'addSpellCritChance', 'addSpellCritMultiplier'],
		mid: ['int', 'int', 'int', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritMultiplier'],
		high: ['int', 'int', 'int', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritMultiplier', 'addSpellCritMultiplier']
	},
	itemTypes: {
		head: 'Cowl',
		neck: 'Amulet',
		chest: 'Robe',
		finger: 'Loop',
		hands: 'Gloves',
		waist: 'Sash',
		legs: 'Pants',
		feet: 'Boots',
		trinket: 'Quartz Fragment',
		twoHanded: 'Gnarled Staff'
	},
	runes: ['magic missile', 'fireblast', 'ice spear', 'tranquility'],
	extraConfig: {
		twoHanded: {
			spellQuality: 4
		}
	},
	passives: [
		80, 50, 52, 72, 56, 64, 66,
		67, 68, 69, 70, 74, 45, 90,
		71, 65, 59, 102, 60, 61, 57
	],
	itemQuality: 4
}, {
	spirit: 'owl',
	build: 'healer',
	itemStats: {
		entry: ['int', 'regenMana'],
		low: ['int', 'regenMana', 'addSpellCritChance', 'addSpellCritMultiplier'],
		mid: ['int', 'int', 'regenMana', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritMultiplier'],
		high: ['int', 'int', 'regenMana', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritChance', 'addSpellCritMultiplier', 'addSpellCritMultiplier']
	},
	itemTypes: {
		head: 'Cowl',
		neck: 'Amulet',
		chest: 'Robe',
		finger: 'Loop',
		hands: 'Gloves',
		waist: 'Sash',
		legs: 'Pants',
		feet: 'Boots',
		trinket: 'Quartz Fragment',
		twoHanded: 'Gnarled Staff'
	},
	runes: ['healing touch', 'healing touch', 'consecrate', 'tranquility'],
	extraConfig: {
		twoHanded: {
			spellQuality: 4
		}
	},
	passives: [
		80, 50, 52, 72, 56, 64, 66,
		67, 68, 69, 70, 74, 45, 90,
		71, 65, 59, 102, 60, 61, 57
	],
	itemQuality: 4
}, {
	spirit: 'lynx',
	build: 'dps',
	itemStats: {
		entry: ['dex', 'dex'],
		low: ['dex', 'dex', 'addAttackCritChance', 'addAttackCritMultiplier'],
		mid: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier'],
		high: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier', 'addAttackCritMultiplier']
	},
	itemTypes: {
		head: 'Leather Cap',
		neck: 'Locket',
		chest: 'Leather Armor',
		finger: 'Loop',
		hands: 'Leather Gloves',
		waist: 'Leather Belt',
		legs: 'Leather Pants',
		feet: 'Leather Boots',
		trinket: 'Dragon Fang',
		oneHanded: 'Dagger',
		offHand: 'Gilded Shield'
	},
	runes: ['smokebomb', 'ambush', 'crystal spikes', 'swiftness'],
	extraConfig: {
		oneHanded: {
			spellQuality: 4
		}
	},
	passives: [
		105, 15, 25, 27, 28, 29, 31,
		32, 75, 76, 77, 78, 79, 81,
		82, 83, 34, 35, 33, 84, 88
	],
	itemQuality: 4
}, {
	spirit: 'lynx',
	build: 'ranged',
	itemStats: {
		entry: ['dex', 'dex'],
		low: ['dex', 'dex', 'addAttackCritChance', 'addAttackCritMultiplier'],
		mid: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier'],
		high: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier', 'addAttackCritMultiplier']
	},
	itemTypes: {
		head: 'Leather Cap',
		neck: 'Locket',
		chest: 'Leather Armor',
		finger: 'Viridian Band',
		hands: 'Leather Gloves',
		waist: 'Leather Belt',
		legs: 'Leather Pants',
		feet: 'Leather Boots',
		trinket: 'Smokey Orb',
		twoHanded: 'Bow'
	},
	runes: ['ambush', 'crystal spikes', 'roll', 'swiftness'],
	extraConfig: {
		oneHanded: {
			spellQuality: 4
		}
	},
	passives: [
		105, 15, 25, 27, 28, 29, 31,
		32, 75, 76, 77, 78, 79, 81,
		82, 83, 34, 35, 33, 84, 88
	],
	itemQuality: 4
}, {
	spirit: 'lynx',
	build: 'deaganManaRanged',
	itemStats: {
		entry: ['dex', 'dex'],
		low: ['dex', 'dex', 'addAttackCritChance', 'regenMana'],
		mid: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'regenMana'],
		high: ['dex', 'dex', 'dex', 'addAttackCritChance', 'addAttackCritChance', 'regenMana', 'regenMana', 'addAttackCritMultiplier']
	},
	itemTypes: {
		head: 'Leather Cap',
		neck: 'Locket',
		chest: 'Leather Armor',
		finger: 'Loop',
		hands: 'Leather Gloves',
		waist: 'Leather Belt',
		legs: 'Leather Pants',
		feet: 'Leather Boots',
		trinket: 'Dragon Fang',
		twoHanded: 'Bow'

	},
	runes: ['smokebomb', 'ambush', 'tranquility', 'tranquility'],
	extraConfig: {
		oneHanded: {
			spellQuality: 4
		}
	},
	passives: [
		105, 15, 25, 27, 28, 29, 31,
		32, 75, 76, 77, 78, 79, 81,
		82, 83, 34, 35, 33, 84, 88
	],
	itemQuality: 4
}, {
	spirit: 'bear',
	build: 'arkieDps',
	itemStats: {
		entry: ['str', 'addAttackCritChance'],
		low: ['str', 'str', 'addAttackCritChance', 'addAttackCritMultiplier'],
		mid: ['str', 'str', 'str', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier'],
		high: ['str', 'str', 'str', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritChance', 'addAttackCritMultiplier', 'addAttackCritMultiplier']
	},
	itemTypes: {
		head: 'Helmet',
		neck: 'Pendant',
		chest: 'Breastplate',
		finger: 'Signet',
		hands: 'Gauntlets',
		waist: 'Belt',
		legs: 'Legplates',
		feet: 'Steel Boots',
		trinket: 'Forged Ember',
		twoHanded: 'Axe'
	},
	runes: ['slash', 'charge', 'whirlwind', 'harvest life'],
	extraConfig: {
		twoHanded: {
			spellQuality: 4
		}
	},
	passives: [
		104, 0, 1, 2, 3, 8, 6,
		4, 5, 62, 97, 96, 95, 63,
		25, 15, 75, 76, 27, 31, 32
	],
	itemQuality: 4
}];

const cleanInventory = ({ inventory }) => {
	const ids = inventory.items.map(({ id }) => id);

	ids.forEach(id => inventory.destroyItem({ itemId: id }, null, true));
};

const setSpirit = (obj, spirit) => {
	obj.class = spirit;
};

const getItems = ({ inventory }, level, { itemMaterials, itemTypes, itemStats, itemQuality, extraConfig, runes }, tier) => {
	const tierStats = itemStats[tier] ?? itemStats.high;

	Object.keys(itemTypes).forEach(slot => {
		const slotConfig = extraConfig[slot] || {};

		const itemConfig = {
			stats: tierStats,
			quality: itemQuality,
			level,
			slot,
			statCount: tierStats.length,
			type: itemTypes[slot],
			...slotConfig
		};

		const iterations = doubleSlots.includes(slot) ? 2 : 1;
		for (let i = 0; i < iterations; i++) {
			const item = itemGenerator.generate(itemConfig);

			//We don't care about attribute requirements
			delete item.requires;

			//Auto equip
			item.eq = true;

			inventory.getItem(item);
		}
	});

	runes.forEach(r => {
		const runeConfig = {
			spell: true,
			spellName: r,
			quality: itemQuality,
			level
		};

		const rune = itemGenerator.generate(runeConfig);

		//Auto equip
		rune.eq = true;

		inventory.getItem(rune);
	});
};

const setLevel = ({ serverId, stats, syncer }, level) => {
	const values = stats.values;

	const oldLevel = values.level;

	values.level = level;

	let delta = level - oldLevel;

	values.hpMax += (40 * delta);

	syncer.setObject(true, 'stats', 'values', 'level', level);
	syncer.setObject(true, 'stats', 'values', 'hpMax', values.hpMax);

	process.send({
		method: 'object',
		serverId: serverId,
		obj: { level }
	});

	stats.calcXpMax();
};

const setPassives = ({ passives }, { passives: passivesConfig }) => {
	passives.untickNode();

	passivesConfig.forEach(p => {
		passives.tickNode({ nodeId: p });
	});
};

//We can't use arrow notation for this because that would mean we don't have scope of the player object
module.exports = function (command) {
	const { spirit = 'bear', build = 'tank', level = balance.maxLevel, clean = false, tier = 'high' } = command;

	const useLevel = Math.max(1, Math.min(balance.maxLevel, ~~level));

	const { obj } = this;

	let blueprint = blueprints.find(c => c.spirit === spirit && c.build === build);
	if (!blueprint)
		blueprint = blueprints.find(c => c.spirit === spirit);

	if (!blueprint) {
		this.notifySelf({ message: 'Build not found' });

		return;
	}

	if (clean)
		cleanInventory(obj);

	setLevel(obj, useLevel);

	setSpirit(obj, spirit);

	getItems(obj, useLevel, blueprint, tier);

	setPassives(obj, blueprint);
};
