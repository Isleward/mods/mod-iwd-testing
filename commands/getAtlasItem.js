//Imports
const eventEmitter = require('../../../misc/events');
const generator = require('../../../items/generator');

//Command Definition
// We can't use arrow notation for this because that would mean we don't have scope of the player object
const getAtlasItem = async function (config) {
	const { id = config, quantity = 1 } = config;

	const { obj } = this;

	if (typeof(id) === 'object') {
		const list = global.itemAtlas
			.getAll('mtx')
			.filter(({ id: itemId }) => {
				const ignore = (
					itemId.includes('constructionMaterial') ||
					itemId.includes('plot') ||
					itemId.includes('accountStash')
				);

				return true;
			})
			.map(i => {
				return {
					id: i.id,
					quantity
				};
			});

		const eventConfig = {
			obj,
			list
		};

		await eventEmitter.emit('onGetMtxList', eventConfig);

		return;
	}

	const blueprint = global.itemAtlas.get(id);

	for (let i = 0; i < quantity; i++) {
		const item = generator.generate(blueprint);

		this.obj.inventory.getItem(item);
	}
};

module.exports = getAtlasItem;
