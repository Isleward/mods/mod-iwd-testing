//Imports
const generator = require('../../../items/generator');
const configSlots = require('../../../items/config/slots');
const factions = require('../../../config/factions');

//Command Definition
// We can't use arrow notation for this because that would mean we don't have scope of the player object
const getItem = async function (config) {
	if (typeof config !== 'object')
		return;

	if (config.slot === 'set') {
		configSlots.slots.forEach(function (s) {
			if (s === 'tool')
				return;

			let newConfig = extend({}, config, {
				slot: s
			});

			this.getItem(newConfig);
		}, this);

		return;
	}

	if (config.stats)
		config.stats = config.stats.split(',');

	if (config.name)
		config.name = config.name.split('_').join(' ');

	if (config.description)
		config.description = config.description.split('_').join(' ');

	if (config.spellName)
		config.spellName = config.spellName.split('_').join(' ');

	if (config.type)
		config.type = config.type.split('_').join(' ');

	if (config.sprite)
		config.sprite = config.sprite.split('_');

	let spritesheet = config.spritesheet;
	delete config.spritesheet;

	let factionList = (config.factions || '').split(',');
	delete config.factions;

	let safe = config.safe;
	delete config.safe;

	let eq = config.eq;
	delete config.eq;

	let item = generator.generate(config);

	if (safe) {
		item.noDrop = true;
		item.noDestroy = true;
		item.noSalvage = true;
	}

	factionList.forEach(function (f) {
		if (f === '')
			return;

		let faction = factions.getFaction(f);
		faction.uniqueStat.generate(item);

		item.factions = [];
		item.factions.push({
			id: f,
			tier: 3
		});
	});

	if (spritesheet)
		item.spritesheet = spritesheet;

	let newItem = this.obj.inventory.getItem(item);

	if (eq)
		this.obj.equipment.equip({ itemId: newItem.id });
};

module.exports = getItem;
