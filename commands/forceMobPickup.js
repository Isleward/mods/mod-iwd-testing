//Config
const radius = 2;
const bagRadius = 2;

//Helper Event
// Fires right before the mob drops loot so we can add items to the list
const onAddForcedItems = function (forceItems, items, killSource) {
	items.push(...forceItems);
};

//Command Definition
// We can't use arrow notation for this because that would mean we don't have scope of the player object
const forceMobPickup = function () {
	const { obj: { x, y, social, instance: { objects: { objects: objList } } } } = this;

	//Find a mob within 'radius' tiles
	const closeMob = objList.find(o => {
		const { mob, x: mx, y: my } = o;

		const match = (
			mob &&
			Math.abs(x - mx) <= radius &&
			Math.abs(y - my) <= radius
		);

		return match;
	});

	if (!closeMob) {
		social.notifySelf({ message: `No mob found within ${radius} tiles.` });

		return;
	}

	const { x: mobX, y: mobY } = closeMob;

	//Find a bag within 'bagRadis' tiles of the mob
	const closeBag = objList.find(o => {
		const { chest, x: bx, y: by } = o;

		const match = (
			chest &&
			chest.ownerName === null &&
			Math.abs(mobX - bx) <= bagRadius &&
			Math.abs(mobY - by) <= bagRadius
		);

		return match;
	});

	if (!closeBag) {
		social.notifySelf({ message: `No valid bag found within ${bagRadius} tiles.` });

		return;
	}

	const { inventory: { items: bagItems } } = closeBag;

	//Add an event handler to the mob to add the new items before dropping a bag
	closeMob.onEvent('onBeforeDropBag', onAddForcedItems.bind(null, bagItems));

	//Destroy the bag
	closeBag.destroyed = true;
};

module.exports = forceMobPickup;
