//Helpers
const getItem = require('./commands/getItem');
const getIdols = require('./commands/getIdols');
const regenMap = require('./commands/regenMap');
const loadBuild = require('./commands/loadBuild');
const getAtlasItem = require('./commands/getAtlasItem');
const forceMobPickup = require('./commands/forceMobPickup');

const balanceTracker = require('./balanceTracker');

//Rest Endpoints
const restRebalance = require('./restEndpoints/rebalance');
const restGetBalance = require('./restEndpoints/getBalance');

//Mod
module.exports = {
	name: 'Toolset: Testing',
	desc: 'Adds commands to assist in testing and balancing encounters',

	init: function () {
		[
			'rebalance',
			'beforeSpawnThread',
			'onBeforeGetCommandRoles',
			'onBeforeRegisterRestEndpoints'
		].forEach(eventName => {
			this.events.on(eventName, this[eventName].bind(this));
		});
	},

	initMapThread: function () {
		global.balanceTracker = balanceTracker;
	},

	rebalance: function ({ mod, contentKey, config }) {
		balanceTracker.set({ mod, contentKey, config });

		cons.emit('events', {
			onGetMessages: [{
				messages: [{
					class: 'color-tealB',
					message: `Rebalanced ${mod}.${contentKey}`
				}]
			}]
		});
	},

	beforeSpawnThread: function ({ thread }) {
		Object.assign(thread.preConfig, {
			balanceTracker: {
				config: balanceTracker.getAll()
			}
		});
	},

	onBeforeGetCommandRoles: function (commandRoles, commandActions) {
		commandRoles.loadBuild = 10;
		commandActions.loadBuild = loadBuild;

		commandRoles.regenMap = 10;
		commandActions.regenMap = regenMap;

		commandRoles.forceMobPickup = 10;
		commandActions.forceMobPickup = forceMobPickup;

		commandRoles.getAtlasItem = 10;
		commandActions.getAtlasItem = getAtlasItem;

		commandRoles.getItem = 10;
		commandActions.getItem = getItem;

		commandRoles.getIdols = 10;
		commandActions.getIdols = getIdols;
	},

	onBeforeRegisterRestEndpoints: function (endpoints) {
		endpoints.mtRebalance = restRebalance;
		endpoints.mtGetBalance = restGetBalance;
	}
};
