//System
const eventEmitter = require('../../../../misc/events');

//Plugins
const bcrypt = require('bcrypt-nodejs');

//Helpers
const onAuthChecked = async (req, res, fn, err, compareResult) => {
	if (!compareResult) {
		res.json({
			success: false,
			error: 'Invalid username/password'
		});

		return;
	}

	const { query: { authUsername } } = req;

	const accountInfo = (await io.getAsync({
		key: authUsername,
		table: 'accountInfo',
		noDefault: true
	})) ?? { level: 0 };

	const msgAccountInfo = {
		username: authUsername,
		accountInfo
	};

	await eventEmitter.emit('onBeforeGetAccountInfo', msgAccountInfo);

	if (msgAccountInfo.accountInfo.level < 8) {
		res.json({
			success: false,
			error: 'Account level too low'
		});

		return;
	}

	await fn(req, res);
};

//This file wraps a rest request into a new function
// This function accesses the username and password query string parameters
// Then ensures that the details are correct
// Then ensures that the user has an account level of 8+ (Mod or higher)
const authWrap = fn => {
	const fnWrapped = (req, res) => {
		try {
			res.append('Access-Control-Allow-Origin', ['*']);
			res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
			res.append('Access-Control-Allow-Headers', 'Content-Type');
	
			(async () => {
				const { query: { authUsername, authPassword } } = req;
				
				if (!authUsername) {
					res.json({
						success: false,
						error: 'No username provided'
					});

					return;
				}

				const storedPassword = await io.getAsync({
					key: authUsername,
					table: 'login'
				});

				bcrypt.compare(authPassword, storedPassword, onAuthChecked.bind(null, req, res, fn));
			})(); 
		} catch (e) {}
	};

	return fnWrapped;
};

module.exports = authWrap;

