//Extenal Helpers
const eventEmitter = require('../../../misc/events');

//Helpers
const authWrap = require('./helper/authWrap');

//Endpoint
module.exports = authWrap(async (req, res) => {
	try {
		const { query: { mod, contentKey, json } } = req;

		const config = JSON.parse(json);

		await eventEmitter.emit('rebalance', {
			mod,
			contentKey,
			config
		});

		res.json({ success: true });
	} catch (e) {
		res.json({ success: false });
	}
});
