//Helpers
const authWrap = require('./helper/authWrap');
const balanceTracker = require('../balanceTracker');

//Endpoint
module.exports = authWrap(async (req, res) => {
	try {
		const result = balanceTracker.getAll();

		res.json({ balanceEntries: result, success: true });
	} catch (e) {
		res.json({ success: false });
	}
});
